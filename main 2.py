#Première ligne ; on demande à harry dans quelle boutique il se rends aujourd'hui.
#La réponse va déterminer dans quelle boutique il se rends et par consequent quelle fonction va etre utilisée. 
choix_boutique = int(input("Dans quelle boutique Harry se rends-il ? : (Fleury : 1, Guipure : 2, Ollivander : 3) "))
dico_de_la_monnaie_dispo = {200 : 1, 100 : 3, 50 : 1, 20 : 2, 10 : 1, 5 : 2, 2 : 5, 1 : 0} 

if choix_boutique == 1: 
  #On mets donc la fonction numéro 1
  monnaie = int(input("combien de monnaie doit-t-on rendre(en euros) ? "))
  def rendu_de_monnaie1(monnaie_a_rendre):
    dico_monnaie = {
        '200': 0,
        '100': 0,
        '50': 0,
        '20': 0,
        '10': 0,
        '5': 0,
        '2': 0,
        '1': 0
    }  
    dico_monnaie['200'] = monnaie_a_rendre // 200
    monnaie_a_rendre %= 200
    dico_monnaie['100'] = monnaie_a_rendre // 100
    monnaie_a_rendre %= 100
    dico_monnaie['50'] = monnaie_a_rendre // 50
    monnaie_a_rendre %= 50
    dico_monnaie['20'] = monnaie_a_rendre // 20
    monnaie_a_rendre %= 20
    dico_monnaie['10'] = monnaie_a_rendre // 10
    monnaie_a_rendre %= 10
    dico_monnaie['5'] = monnaie_a_rendre // 5
    monnaie_a_rendre %= 5
    dico_monnaie['2'] = monnaie_a_rendre // 2
    monnaie_a_rendre %= 2
    dico_monnaie['1'] = monnaie_a_rendre // 1
    monnaie_a_rendre %= 1
    return (dico_monnaie)

  print(rendu_de_monnaie1(monnaie))

elif choix_boutique == 2:
  for i in range (5):   
    monnaie = int(input("combien de monnaie doit-t-on rendre(en euros) ? "))
    def rendu_de_monnaie2(monnaie_a_rendre, dico_monnaie_disponible):
      dico_monnaie = {}
      liste_taille_billets = [200, 100, 50, 20, 10, 5, 2, 1]  
      for taille in liste_taille_billets:  # taille va être égale à 200, puis à 100, ...
          if monnaie_a_rendre // taille <= dico_monnaie_disponible[
                  taille]:  #si il y a assez de billets de cette taille alors ...
              dico_monnaie[taille] = monnaie_a_rendre // taille
              dico_monnaie_disponible[taille] -=  monnaie_a_rendre // taille
              monnaie_a_rendre %= taille
          else:
              monnaie_a_rendre -= dico_monnaie_disponible[taille] * taille
              dico_monnaie[taille] = dico_monnaie_disponible[taille]
              dico_monnaie_disponible[taille] = 0
      if monnaie_a_rendre > 0 :
        print ("pas assez de monnaie")
      else : 
        return (dico_monnaie)

    print(rendu_de_monnaie2 (monnaie, dico_de_la_monnaie_dispo))
    
elif choix_boutique == 3:
  #et donc la la troisièmes
  
  print("pas encore fait")

else :
  print("Veuillez rentrer un nombre correct!")

#Suite à cela on fait une interface clean qui nous inscrivera les valeurs correspondantes. 
#On fera également en sorte que l'utilisateur puisse entrer plusieurs valeurs à la suite pour une seule boutique et que le programme ne se relance pas à chaque fois pour que l'utilisateur ne soit pas obligé de remmettre son choix de boutique à chaque valeur rentrée. 
